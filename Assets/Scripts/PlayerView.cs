﻿using UnityEngine;

public class PlayerView : BaseCharacterView
{
    [SerializeField] private PlayerInputController playerInputController;
    [SerializeField] private CameraMouseRotator cameraMouseRotator;

    protected override void OnInitialize()
    {
        base.OnInitialize();
        cameraMouseRotator.OnCameraRotatedEvent += playerInputController.SetHeadRotation;
    }

    protected override void OnUninitialize()
    {
        base.OnUninitialize();
        cameraMouseRotator.OnCameraRotatedEvent -= playerInputController.SetHeadRotation;
    }
}
