﻿using System;
using UnityEngine;

[Serializable]
public abstract class BaseCharacterModel : ScriptableObject, IDestroyable
{
    [SerializeField] private StatModel health;
    public StatModel Health { get; } = new StatModel();
    public float MovementSpeed { get; private set; }
}