﻿using UnityEngine;

public class PlayerInputController : MonoBehaviour
{
    [SerializeField] private float forwardMovingSpeed;
    [SerializeField] private float backwardMovingSpeed;
    [SerializeField] private float rotatingSpeed;
    [SerializeField] private Transform head;
    [SerializeField] private KeyCode shootKey;

    private void Update()
    {
        float verticalInput = Input.GetAxis("Vertical");
        float horizontalInput = Input.GetAxis("Horizontal");

        float speed = (verticalInput > 0) ? forwardMovingSpeed : backwardMovingSpeed;
        float offset = speed * verticalInput;

        if (offset != 0f)
        {
            transform.position += transform.forward * offset;
        }

        transform.eulerAngles = transform.eulerAngles + Vector3.up * horizontalInput * verticalInput;

        if (Input.GetKeyDown(shootKey))
        {
            Shoot();
        }
    }

    private void Shoot()
    {

    }

    public void SetHeadRotation(Vector3 headEueler)
    {
        head.transform.eulerAngles = headEueler.y * Vector3.up;
    }
}
