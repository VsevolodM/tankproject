﻿using System;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraMouseRotator : MonoBehaviour
{
    public event Action<Vector3> OnCameraRotatedEvent;

    [SerializeField] private Transform target;
    [SerializeField] private float scaleSensitivity;
    [SerializeField] private Vector3 offset;

    private Camera camera;

    private void Awake()
    {
        camera = GetComponent<Camera>();
    }

    private void Update()
    {
        float xRotation = Input.GetAxis("Mouse X");
        camera.transform.RotateAround(target.transform.position, Vector3.up, xRotation);

        if (xRotation != 0f)
        {
            OnCameraRotatedEvent?.Invoke(camera.transform.eulerAngles);
        }

        float mouseWheelInput = Input.GetAxis("Mouse ScrollWheel");

        if (mouseWheelInput != 0f)
        {
            camera.transform.position += transform.forward * mouseWheelInput * scaleSensitivity;
        }

        Vector3 moveOffset = target.transform.position - transform.position - offset;
        Debug.Log("moveOffset: " + moveOffset);
        camera.transform.position += moveOffset;
    }
}
