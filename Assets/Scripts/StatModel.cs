﻿using System;
using UnityEngine;

[Serializable]
public class StatModel
{
    public event Action OnChangedEvent;

    [SerializeField] private StatParameterModel currentValue = new StatParameterModel();
    [SerializeField] private StatParameterModel maximumValue = new StatParameterModel();

    public StatParameterModel CurrentValue
    {
        get => currentValue;
        set => currentValue = value;
    }

    public StatParameterModel MaximumValue
    {
        get => maximumValue;
        set => maximumValue = value;
    }

    public StatModel()
    {
        CurrentValue.OnChangedEvent += OnChanged;
        MaximumValue.OnChangedEvent += OnChanged;
    }

    ~StatModel()
    {
        CurrentValue.OnChangedEvent -= OnChanged;
        MaximumValue.OnChangedEvent -= OnChanged;
    }

    private void OnChanged()
    {
        OnChangedEvent?.Invoke();
    }
}