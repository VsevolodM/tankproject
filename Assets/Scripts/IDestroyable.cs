﻿public interface IDestroyable
{
    StatModel Health { get; }
}
