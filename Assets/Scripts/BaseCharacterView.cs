﻿using UnityEngine;

public abstract class BaseCharacterView : BaseView<BaseCharacterModel>
{
    [SerializeField] private StatView healthView;

    protected override void OnInitialize()
    {
        base.OnInitialize();
        //healthView.Initialize(Model.Health);
    }
}

public abstract class BaseView<T> : MonoBehaviour, IView<T>
{
    public T Model { get; private set; }

    public void Initialize(T model)
    {
        Model = model;
        OnInitialize();
    }

    protected virtual void OnInitialize()
    {

    }

    private void OnDestroy()
    {
        Uninitialize();
    }

    private void Uninitialize()
    {
        Model = default(T);
        OnUninitialize();
    }

    protected virtual void OnUninitialize()
    {

    }
}

public interface IView<T>
{
    void Initialize(T model);
}