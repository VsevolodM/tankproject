﻿using UnityEngine;

public class EntryPoint : MonoBehaviour
{
    private void Awake()
    {
        PlayerView player = FindObjectOfType<PlayerView>();
        player.Initialize(new PlayerModel());
    }
}
